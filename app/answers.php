<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class answers extends Model
{
    public function questions()
    {
      return $this->belongsToMany('App\questions');
    }

    public function questionnairs()
    {
      return $this->belongsToMany('App\questionnaires');
    }
}
