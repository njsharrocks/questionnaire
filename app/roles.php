<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    public function permissions() {
      return $this->belongsToMany(permissions::class);
    }

    public function givePermissionsTo(permissions $permissions) {
      return $this->permissions()->sync($permissions);
    }
}
