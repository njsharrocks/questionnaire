<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaires extends Model
{

    public $timestamps = false;

    protected $fillable = [
      'title',
      'QDescription',
      'ADescription1',
      'ADescription2',
      'ADescription3',
      'ADescription4'

    ];

    public function questions() {
      return $this->belongsToMany('App\questions');
    }

    public function answers() {
      return $this->belongsToMany('App\answers');
    }

    public function user() {
      return $this->belongsTo('App\User');
    }
}
