<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questions extends Model
{
    public function answers()
    {
      return $this->belongsToMany('App\answers');
    }

    public function questionnaires()
    {
      return $this->belongsToMany('App\questionnaires');
    }
}
