<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
          ['id' => 1, 'QDescription' =>"What style of questionnaire are you happiest to take part in?"],
          ['id' => 2, 'QDescription' =>"How much time would you typically be willing to spare on a questionnaire?"],
          ['id' => 3, 'QDescription' =>"How many questions would you typically be willing to answer on a questionnaire?"],
          ['id' => 4, 'QDescription' =>"How do you feel about personal details being taken as part of a questionnaire?"],
          ['id' => 5, 'QDescription' =>"How would you prefer to take a questionnaire?"],
        ]);
    }
}
