<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Questionnaire</title>
</head>
<body>
  @extends('layouts.app')

  @section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                  <div class="panel-heading">Questionnaire</div>

                  <div class="panel-body">
                  <section>
                    @if (isset ($questionnaires))

                      <ul>
                        @foreach ($questionnaires as $questionnaire)
                          <li><a href="/admin/questionnaire/{{ $questionnaire->id}}" name="{{ $questionnaire->title}}">{{ $questionnaire->Title }}</a></li>
                        @endforeach
                      </ul>
                    @else
                      <p> No Questionnaire found </p>
                    @endif
                  </section>

                  {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
                    <div class="row">
                      {!! Form::submit('Submit Questionnaire', ['class' => 'button']) !!}
                    </div>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
</body>
</html>
