<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Data</title>
</head>
<body>
  <h1>Data</h1>

  <section>
    @if (isset ($questionnaires))

      <ul>
        @foreach ($questionnaires as $questionnaire)
          <li>{{ $questionnaire->title}}</li>
        @endforeach
      </ul>
    @else
      <p> No Data found </p>
    @endif
  </section>

</body>
</html>
