<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Create Questions</title>
</head>
<body>
  @extends('layouts.app')

  @section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                  <div class="panel-heading">Create Questions</div>

                  <div class="panel-body">

                    {!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
                          {{ csrf_field() }}
                        <div class="row large-12 columns">
                            {!! Form::label('Title', 'Title:') !!}
                            {!! Form::text('Title', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('QDescription', 'Question:') !!}
                            {!! Form::text('QDescription', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription1', 'Answer:') !!}
                            {!! Form::text('ADescription1', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription2', 'Answer:') !!}
                            {!! Form::text('ADescription2', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription3', 'Answer:') !!}
                            {!! Form::text('ADescription3', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription4', 'Answer:') !!}
                            {!! Form::text('ADescription4', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('QDescription', 'Question:') !!}
                            {!! Form::text('QDescription', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription1', 'Answer:') !!}
                            {!! Form::text('ADescription1', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription2', 'Answer:') !!}
                            {!! Form::text('ADescription2', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription3', 'Answer:') !!}
                            {!! Form::text('ADescription3', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription4', 'Answer:') !!}
                            {!! Form::text('ADescription4', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('QDescription', 'Question:') !!}
                            {!! Form::text('QDescription', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription1', 'Answer:') !!}
                            {!! Form::text('ADescription1', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription2', 'Answer:') !!}
                            {!! Form::text('ADescription2', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription3', 'Answer:') !!}
                            {!! Form::text('ADescription3', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription4', 'Answer:') !!}
                            {!! Form::text('ADescription4', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('QDescription', 'Question:') !!}
                            {!! Form::text('QDescription', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription1', 'Answer:') !!}
                            {!! Form::text('ADescription1', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription2', 'Answer:') !!}
                            {!! Form::text('ADescription2', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription3', 'Answer:') !!}
                            {!! Form::text('ADescription3', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription4', 'Answer:') !!}
                            {!! Form::text('ADescription4', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('QDescription', 'Question:') !!}
                            {!! Form::text('QDescription', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription1', 'Answer:') !!}
                            {!! Form::text('ADescription1', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription2', 'Answer:') !!}
                            {!! Form::text('ADescription2', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription3', 'Answer:') !!}
                            {!! Form::text('ADescription3', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('ADescription4', 'Answer:') !!}
                            {!! Form::text('ADescription4', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-4 columns">
                            {!! Form::submit('Create Questionnaire', ['class' => 'button']) !!}
                        </div>
                    {!! Form::close() !!}
                  </div>
              </div>
          </div>
      </div>
  </div>
  @endsection
</body>
</html>
