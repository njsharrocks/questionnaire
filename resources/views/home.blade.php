@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                  <section>
                  </section>
                    {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
                      <div class="row">
                        {!! Form::submit('Create Questionnaire', ['class' => 'button']) !!}
                      </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
